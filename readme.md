```plantuml
@startuml
client -> middService: create order request
middService -> middProcess: create order
activate middProcess

middProcess -> MessageInspector: check message
activate MessageInspector
MessageInspector --> middProcess: response
deactivate MessageInspector

alt successful case
    middProcess -> UM: check user zp nhận tiền
    activate UM
    UM --> middProcess: response
	deactivate UM

    alt successful case
        middProcess -> TPE:  create order
        activate TPE
        TPE --> middProcess: response
        deactivate TPE
    end
end

loop query status
    client -> middService: query status
    middService -> middProcess: query status
    client <-- middService: status
    break receive final state
    end
end
deactivate middProcess
@enduml
```